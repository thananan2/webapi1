﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Configuration;
using webapi.DAL;
using webapi.Models;

namespace webapi1.BL
{
    public class FakeBL
    {
        public ResponseModel testFakeAPI()
        {
            ResponseModel response = new ResponseModel();
            try
            {
                string Url_test1 = ConfigurationManager.AppSettings["Url_test1"];
                var res = getRequest(Url_test1);
                var itemList = JsonConvert.DeserializeObject<List<FakeModel>>(res);

                response.data = itemList;
                response.status = "success";
                response.msg = "success";
            }
            catch (Exception ex)
            {
                response.data = "";
                response.status = "error try catch";
                response.msg = ex.ToString();
                throw;
            }

            return response;
        }

        private string getRequest(string url) 
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            return responseString;
        }

        private string postRequest() 
        {
            return "";
        }
    }
}