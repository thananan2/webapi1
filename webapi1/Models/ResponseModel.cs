﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webapi.Models
{
    public class ResponseModel
    {
        public object data { get; set; }
        public string msg { get; set; }
        public string status { get; set; }
    }
}