﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webapi.Models;
using webapi1.Service;

namespace webapi1.Controllers
{
    public class FakeController : ApiController
    {
        // GET api/values
        public ResponseModel GetData()
        {
            FakeService _fake = new FakeService();
            return _fake.callFakeData();
        }
    }
}
